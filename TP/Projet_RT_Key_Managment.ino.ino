#include <LBattery.h>
#include <rgb_lcd.h>

char buff[256];
int buzzerPin = A0; //Define buzzerPin
rgb_lcd lcd ;

void setup() {
  // Commence la connection avec l'ecran
  //lcd.begin(16, 2);
  //Ecrire sur l'ecran 
  //lcd.print("hello, world!");
  //C'est pour le port serie et la led
  Serial.begin(115200);
  pinMode(13,OUTPUT);
  pinMode(buzzerPin, OUTPUT); //Set buzzerPin as output
}

void loop() {
  //afficher le niveau de batterie et si la batterie et en charge ou pas 
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
 
  if(LBattery.isCharging() == 0){
    Serial.println("Charger le LinkitOne");
  }else{
    Serial.println("LinkitOne Charger");
  }
  
  //niveau de batterie
  if(LBattery.level() == 100){
    digitalWrite(13,HIGH);
    delay(100);
    digitalWrite(13,LOW);
    delay(100);

    beep(50);
    
  }else{
    Serial.println("0%");
  }

   delay(1000); 
}

//Beeper battrery pour faire sonner le buzzer
void beep(unsigned char delayms) { 
  analogWrite(buzzerPin, 500); 
  delay(delayms); 
  analogWrite(buzzerPin, 0); 
  delay(delayms); 
  
}